import Recipe from './Recipe'
import '../../stylesheets/Menu.css'

const Menu = ({ recipes }) =>
  <article>
    <header>
      <h1>Low-Carb Recipes</h1>
    </header>
    <div className = "recipes">
      { recipes.map((recipe, i) =>
        <Recipe key={i} {...recipe} />)
      }
    </div>
  </article>

export default Menu
